require 'test_helper'

class MeetingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @meeting = meetings(:one)
  end

  test "should get index" do
    get meetings_url
    assert_response :success
  end

  test "should get new" do
    get new_meeting_url
    assert_response :success
  end

  test "should create meeting" do
    assert_difference('Meeting.count') do
      post meetings_url, params: { meeting: { candidate_id: @meeting.candidate_id, data_state: @meeting.data_state, date_end_meeting: @meeting.date_end_meeting, date_start: @meeting.date_start, date_start_meeting: @meeting.date_start_meeting, report_id: @meeting.report_id, room: @meeting.room, user_id: @meeting.user_id } }
    end

    assert_redirected_to meeting_url(Meeting.last)
  end

  test "should show meeting" do
    get meeting_url(@meeting)
    assert_response :success
  end

  test "should get edit" do
    get edit_meeting_url(@meeting)
    assert_response :success
  end

  test "should update meeting" do
    patch meeting_url(@meeting), params: { meeting: { candidate_id: @meeting.candidate_id, data_state: @meeting.data_state, date_end_meeting: @meeting.date_end_meeting, date_start: @meeting.date_start, date_start_meeting: @meeting.date_start_meeting, report_id: @meeting.report_id, room: @meeting.room, user_id: @meeting.user_id } }
    assert_redirected_to meeting_url(@meeting)
  end

  test "should destroy meeting" do
    assert_difference('Meeting.count', -1) do
      delete meeting_url(@meeting)
    end

    assert_redirected_to meetings_url
  end
end
