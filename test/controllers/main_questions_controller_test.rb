require 'test_helper'

class MainQuestionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @main_question = main_questions(:one)
  end

  test "should get index" do
    get main_questions_url
    assert_response :success
  end

  test "should get new" do
    get new_main_question_url
    assert_response :success
  end

  test "should create main_question" do
    assert_difference('MainQuestion.count') do
      post main_questions_url, params: { main_question: { custom: @main_question.custom, level: @main_question.level, pool_id: @main_question.pool_id, subject: @main_question.subject } }
    end

    assert_redirected_to main_question_url(MainQuestion.last)
  end

  test "should show main_question" do
    get main_question_url(@main_question)
    assert_response :success
  end

  test "should get edit" do
    get edit_main_question_url(@main_question)
    assert_response :success
  end

  test "should update main_question" do
    patch main_question_url(@main_question), params: { main_question: { custom: @main_question.custom, level: @main_question.level, pool_id: @main_question.pool_id, subject: @main_question.subject } }
    assert_redirected_to main_question_url(@main_question)
  end

  test "should destroy main_question" do
    assert_difference('MainQuestion.count', -1) do
      delete main_question_url(@main_question)
    end

    assert_redirected_to main_questions_url
  end
end
