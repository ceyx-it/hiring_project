# Preview all emails at http://localhost:3000/rails/mailers/result_mailer
class ResultMailerPreview < ActionMailer::Preview

  def sample_mail_preview
    ResultMailer.result_email(Candidate.all.first)
  end

end
