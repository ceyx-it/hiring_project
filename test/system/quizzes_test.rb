require "application_system_test_case"

class QuizzesTest < ApplicationSystemTestCase
  setup do
    @quiz = quizzes(:one)
  end

  test "visiting the index" do
    visit quizzes_url
    assert_selector "h1", text: "Quizzes"
  end

  test "creating a Quiz" do
    visit quizzes_url
    click_on "New Quiz"

    fill_in "Candidate", with: @quiz.candidate_id
    fill_in "Client", with: @quiz.client_id
    fill_in "Pool", with: @quiz.pool_id
    fill_in "Score Final", with: @quiz.score_final
    fill_in "Score Max", with: @quiz.score_max
    click_on "Create Quiz"

    assert_text "Quiz was successfully created"
    click_on "Back"
  end

  test "updating a Quiz" do
    visit quizzes_url
    click_on "Edit", match: :first

    fill_in "Candidate", with: @quiz.candidate_id
    fill_in "Client", with: @quiz.client_id
    fill_in "Pool", with: @quiz.pool_id
    fill_in "Score Final", with: @quiz.score_final
    fill_in "Score Max", with: @quiz.score_max
    click_on "Update Quiz"

    assert_text "Quiz was successfully updated"
    click_on "Back"
  end

  test "destroying a Quiz" do
    visit quizzes_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Quiz was successfully destroyed"
  end
end
