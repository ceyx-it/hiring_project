require "application_system_test_case"

class ReportsTest < ApplicationSystemTestCase
  setup do
    @report = reports(:one)
  end

  test "visiting the index" do
    visit reports_url
    assert_selector "h1", text: "Reports"
  end

  test "creating a Report" do
    visit reports_url
    click_on "New Report"

    fill_in "Advise", with: @report.advise
    fill_in "Bad Point", with: @report.bad_point
    fill_in "Best Point", with: @report.best_point
    fill_in "Data State", with: @report.data_state
    fill_in "Meeting", with: @report.meeting_id
    click_on "Create Report"

    assert_text "Report was successfully created"
    click_on "Back"
  end

  test "updating a Report" do
    visit reports_url
    click_on "Edit", match: :first

    fill_in "Advise", with: @report.advise
    fill_in "Bad Point", with: @report.bad_point
    fill_in "Best Point", with: @report.best_point
    fill_in "Data State", with: @report.data_state
    fill_in "Meeting", with: @report.meeting_id
    click_on "Update Report"

    assert_text "Report was successfully updated"
    click_on "Back"
  end

  test "destroying a Report" do
    visit reports_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Report was successfully destroyed"
  end
end
