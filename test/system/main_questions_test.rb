require "application_system_test_case"

class MainQuestionsTest < ApplicationSystemTestCase
  setup do
    @main_question = main_questions(:one)
  end

  test "visiting the index" do
    visit main_questions_url
    assert_selector "h1", text: "Main Questions"
  end

  test "creating a Main question" do
    visit main_questions_url
    click_on "New Main Question"

    fill_in "Custom", with: @main_question.custom
    fill_in "Level", with: @main_question.level
    fill_in "Pool", with: @main_question.pool_id
    fill_in "Subject", with: @main_question.subject
    click_on "Create Main question"

    assert_text "Main question was successfully created"
    click_on "Back"
  end

  test "updating a Main question" do
    visit main_questions_url
    click_on "Edit", match: :first

    fill_in "Custom", with: @main_question.custom
    fill_in "Level", with: @main_question.level
    fill_in "Pool", with: @main_question.pool_id
    fill_in "Subject", with: @main_question.subject
    click_on "Update Main question"

    assert_text "Main question was successfully updated"
    click_on "Back"
  end

  test "destroying a Main question" do
    visit main_questions_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Main question was successfully destroyed"
  end
end
