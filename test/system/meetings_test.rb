require "application_system_test_case"

class MeetingsTest < ApplicationSystemTestCase
  setup do
    @meeting = meetings(:one)
  end

  test "visiting the index" do
    visit meetings_url
    assert_selector "h1", text: "Meetings"
  end

  test "creating a Meeting" do
    visit meetings_url
    click_on "New Meeting"

    fill_in "Candidate", with: @meeting.candidate_id
    fill_in "Data State", with: @meeting.data_state
    fill_in "Date End Meeting", with: @meeting.date_end_meeting
    fill_in "Date Start", with: @meeting.date_start
    fill_in "Date Start Meeting", with: @meeting.date_start_meeting
    fill_in "Report", with: @meeting.report_id
    fill_in "Room", with: @meeting.room
    fill_in "User", with: @meeting.user_id
    click_on "Create Meeting"

    assert_text "Meeting was successfully created"
    click_on "Back"
  end

  test "updating a Meeting" do
    visit meetings_url
    click_on "Edit", match: :first

    fill_in "Candidate", with: @meeting.candidate_id
    fill_in "Data State", with: @meeting.data_state
    fill_in "Date End Meeting", with: @meeting.date_end_meeting
    fill_in "Date Start", with: @meeting.date_start
    fill_in "Date Start Meeting", with: @meeting.date_start_meeting
    fill_in "Report", with: @meeting.report_id
    fill_in "Room", with: @meeting.room
    fill_in "User", with: @meeting.user_id
    click_on "Update Meeting"

    assert_text "Meeting was successfully updated"
    click_on "Back"
  end

  test "destroying a Meeting" do
    visit meetings_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Meeting was successfully destroyed"
  end
end
