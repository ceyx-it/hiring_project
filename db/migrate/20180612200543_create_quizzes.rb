class CreateQuizzes < ActiveRecord::Migration[5.2]
  def change
    create_table :quizzes do |t|
      t.integer :score_final
      t.integer :score_max
      t.references :pool, foreign_key: true
      t.references :client, foreign_key: true
      t.references :candidate, foreign_key: true

      t.timestamps
    end
  end
end
