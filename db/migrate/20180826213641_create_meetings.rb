class CreateMeetings < ActiveRecord::Migration[5.2]
  def change
    create_table :meetings do |t|
      t.integer :candidate_id
      t.integer :report_id
      t.integer :user_id
      t.datetime :date_start
      t.integer :date_start_meeting
      t.integer :date_end_meeting
      t.string :room
      t.integer :data_state

      t.timestamps
    end
  end
end
