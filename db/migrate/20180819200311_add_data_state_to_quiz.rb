class AddDataStateToQuiz < ActiveRecord::Migration[5.2]
  def change
    add_column :quizzes, :data_state, :integer
  end
end
