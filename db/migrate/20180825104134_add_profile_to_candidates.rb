class AddProfileToCandidates < ActiveRecord::Migration[5.2]
  def change
    add_column :candidates, :profile_id, :integer
  end
end
