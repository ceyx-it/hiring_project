class AddFieldMovieToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :opt_in_mail, :string
    add_column :users, :opt_in_sms, :string
    add_column :users, :data_state, :integer
  end
end
