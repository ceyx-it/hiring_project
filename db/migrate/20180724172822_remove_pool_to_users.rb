class RemovePoolToUsers < ActiveRecord::Migration[5.2]
  def change
    remove_column :users, :pool_id
  end
end
