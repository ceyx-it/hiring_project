class RemovePoolFromQuestion < ActiveRecord::Migration[5.2]
  def change
    remove_column :questions, :pool_id
  end
end
