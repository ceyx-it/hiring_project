class CreateCandidates < ActiveRecord::Migration[5.2]
  def change
    create_table :candidates do |t|
      t.float :experience
      t.string :email
      t.references :profile, foreign_key: true
      t.integer :data_state

      t.timestamps
    end

    add_reference :candidates, :job_offer, foreign_key: true

  end
end
