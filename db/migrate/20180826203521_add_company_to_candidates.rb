class AddCompanyToCandidates < ActiveRecord::Migration[5.2]
  def change
    add_column :candidates, :company_id, :integer
  end
end
