class AddDataStateToMainQuestion < ActiveRecord::Migration[5.2]
  def change
    add_column :main_questions, :data_state, :integer
  end
end
