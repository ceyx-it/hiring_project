class RemoveDateToMeetings < ActiveRecord::Migration[5.2]
  def change
    remove_column :meetings, :date_start_meeting
    remove_column :meetings, :date_end_meeting
  end
end
