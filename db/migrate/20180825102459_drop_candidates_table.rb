class DropCandidatesTable < ActiveRecord::Migration[5.2]
  def change
    drop_table :candidates, force: :cascade
  end
end
