class AddCommentToMeetings < ActiveRecord::Migration[5.2]
  def change
    add_column :meetings, :comment, :text
  end
end
