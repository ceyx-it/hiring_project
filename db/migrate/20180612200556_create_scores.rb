class CreateScores < ActiveRecord::Migration[5.2]
  def change
    create_table :scores do |t|
      t.float :timer
      t.references :answer, foreign_key: true
      t.references :question, foreign_key: true
      t.references :quiz, foreign_key: true

      t.timestamps
    end
  end
end
