class AddOpenTokToMeetings < ActiveRecord::Migration[5.2]
  def change
    add_column :meetings, :candidate_token_id, :string
    add_column :meetings, :expert_token_id, :string
    add_column :meetings, :path_record, :string
  end
end
