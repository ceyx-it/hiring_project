class CreateQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :questions do |t|
      t.text :content
      t.references :pool, foreign_key: true
      t.references :user, foreign_key: true
      t.references :main_question, foreign_key: true

      t.timestamps
    end
  end
end
