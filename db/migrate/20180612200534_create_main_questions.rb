class CreateMainQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :main_questions do |t|
      t.integer :level
      t.boolean :custom
      t.string :subject
      t.references :pool, foreign_key: true

      t.timestamps
    end
  end
end
