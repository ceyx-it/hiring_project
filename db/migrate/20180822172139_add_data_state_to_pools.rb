class AddDataStateToPools < ActiveRecord::Migration[5.2]
  def change
    add_column :pools, :data_state, :integer
  end
end
