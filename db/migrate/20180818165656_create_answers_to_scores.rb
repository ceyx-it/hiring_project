class CreateAnswersToScores < ActiveRecord::Migration[5.2]
  def change
      create_table :answers_scores, id: false do |t|
        t.belongs_to :answer, index: true
        t.belongs_to :score, index: true
    end
  end
end
