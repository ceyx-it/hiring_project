class CreateJobOffers < ActiveRecord::Migration[5.2]
  def change
    create_table :job_offers do |t|
      t.string :code_offer
      t.string :title
      t.text :comment
      t.string :web_link
      t.string :email
      t.boolean :finish

      t.timestamps
    end
  end
end
