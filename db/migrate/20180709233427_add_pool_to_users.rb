class AddPoolToUsers < ActiveRecord::Migration[5.2]
  def change
    add_reference :users, :pool, foreign_key: true
  end
end
