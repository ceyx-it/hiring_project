class CreateReports < ActiveRecord::Migration[5.2]
  def change
    create_table :reports do |t|
      t.text :advise
      t.text :best_point
      t.text :bad_point
      t.integer :data_state
      t.integer :meeting_id

      t.timestamps
    end
  end
end
