class RemoveAnswerFromScore < ActiveRecord::Migration[5.2]
  def change
    remove_column :scores, :answer_id
  end
end
