# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_09_03_165030) do

  create_table "answers", force: :cascade do |t|
    t.text "content"
    t.integer "point"
    t.boolean "best"
    t.integer "question_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["question_id"], name: "index_answers_on_question_id"
  end

  create_table "answers_scores", id: false, force: :cascade do |t|
    t.integer "answer_id"
    t.integer "score_id"
    t.index ["answer_id"], name: "index_answers_scores_on_answer_id"
    t.index ["score_id"], name: "index_answers_scores_on_score_id"
  end

  create_table "candidates", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.integer "experience"
    t.boolean "opt_in_sms"
    t.boolean "opt_in_mail"
    t.integer "data_state"
    t.integer "profile_id"
    t.integer "company_id"
    t.index ["email"], name: "index_candidates_on_email", unique: true
    t.index ["reset_password_token"], name: "index_candidates_on_reset_password_token", unique: true
  end

  create_table "clients", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.integer "profile_id"
    t.integer "role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "company_id"
    t.index ["company_id"], name: "index_clients_on_company_id"
    t.index ["email"], name: "index_clients_on_email", unique: true
    t.index ["profile_id"], name: "index_clients_on_profile_id"
    t.index ["reset_password_token"], name: "index_clients_on_reset_password_token", unique: true
  end

  create_table "companies", force: :cascade do |t|
    t.string "name"
    t.boolean "activate"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "job_offers", force: :cascade do |t|
    t.string "code_offer"
    t.string "title"
    t.text "comment"
    t.string "web_link"
    t.string "email"
    t.boolean "finish"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "client_id"
    t.index ["client_id"], name: "index_job_offers_on_client_id"
  end

  create_table "main_questions", force: :cascade do |t|
    t.integer "level"
    t.boolean "custom"
    t.string "subject"
    t.integer "pool_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "data_state"
    t.index ["pool_id"], name: "index_main_questions_on_pool_id"
  end

  create_table "meetings", force: :cascade do |t|
    t.integer "candidate_id"
    t.integer "report_id"
    t.integer "user_id"
    t.datetime "date_start"
    t.string "room"
    t.integer "data_state"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "candidate_token_id"
    t.string "expert_token_id"
    t.string "path_record"
    t.text "comment"
  end

  create_table "pools", force: :cascade do |t|
    t.string "subject"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "data_state"
  end

  create_table "pools_users", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "pool_id"
    t.index ["pool_id"], name: "index_pools_users_on_pool_id"
    t.index ["user_id"], name: "index_pools_users_on_user_id"
  end

  create_table "profiles", force: :cascade do |t|
    t.string "gender"
    t.string "first_name"
    t.string "last_name"
    t.date "birth_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "phone"
  end

  create_table "questions", force: :cascade do |t|
    t.text "content"
    t.integer "user_id"
    t.integer "main_question_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["main_question_id"], name: "index_questions_on_main_question_id"
    t.index ["user_id"], name: "index_questions_on_user_id"
  end

  create_table "quizzes", force: :cascade do |t|
    t.integer "score_final"
    t.integer "score_max"
    t.integer "pool_id"
    t.integer "client_id"
    t.integer "candidate_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "data_state"
    t.index ["candidate_id"], name: "index_quizzes_on_candidate_id"
    t.index ["client_id"], name: "index_quizzes_on_client_id"
    t.index ["pool_id"], name: "index_quizzes_on_pool_id"
  end

  create_table "reports", force: :cascade do |t|
    t.text "advise"
    t.text "best_point"
    t.text "bad_point"
    t.integer "data_state"
    t.integer "meeting_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "scores", force: :cascade do |t|
    t.float "timer"
    t.integer "question_id"
    t.integer "quiz_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["question_id"], name: "index_scores_on_question_id"
    t.index ["quiz_id"], name: "index_scores_on_quiz_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.integer "profile_id"
    t.integer "role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "opt_in_mail"
    t.string "opt_in_sms"
    t.integer "data_state"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["profile_id"], name: "index_users_on_profile_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
