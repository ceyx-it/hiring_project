Profile.create!([
                    {gender: nil, first_name: "MATTHIEU", last_name: "LEMONNIER", birth_date: nil}
                ])

Pool.create!([
                 {subject: "Swift"}
             ])


User.create!([
  {email: "admin@ceyx.fr", password:"azerty12"}
])

Company.create!([
                    {name: "La Poste", activate: true}
                ])

Client.create!([
  {email: "client@ceyx.fr", password:"azerty12", company_id: 1}
])

MainQuestion.create!([
                         {level: "junior", custom: false, subject: "Optionnal", pool_id: 1, data_state: "prod"},
                         {level: "junior", custom: false, subject: "Guard", pool_id: 1, data_state: "prod"}
                     ])


Question.create!([
                     {content: "<pre><code> var myArray = [nil, \"Spain\", \"France\", \"England\"]\r\n \r\n for item in myArray {\r\n    print(item ?? \"Unknown\")\r\n }</code></pre>", user_id: 1, main_question_id: 1},
                     {content: "<pre><code> var array = [nil, \"Spain\", \"Paris\", \"England\"]\r\n \r\n for item in array {\r\n    print(item ?? \"Unknown\")\r\n }</code></pre><div><br></div>", user_id: 1, main_question_id: 1},
                     {content: "<pre><code> var paris : String? = \"Paris\"\r\n var array : [String?] = [nil, \"Spain\", paris, \"England\"]\r\n \r\n for i in array {\r\n    print(i ?? \"Unknown\")\r\n }</code></pre><div><br></div>", user_id: 1, main_question_id: 1},
                     {content: "<pre><code>func processData(_ myData : String?) -&gt; String {\r\n    \r\n    guard let myData = myData else {\r\n        return \"&lt;html&gt;Data guard&lt;/html&gt;\"\r\n    }\r\n    \r\n    return \"&lt;html&gt;\" + myData + \"&lt;/html&gt;\"\r\n}\r\n\r\nprint(processData(\"In Data\"))</code></pre>", user_id: 1, main_question_id: 2},
                     {content: "<pre><code>func processData(_ myData : String?) -&gt; String {\r\n    \r\n    guard let myData = myData else {\r\n        return \"&lt;html&gt;Data guard&lt;/html&gt;\"\r\n    }\r\n    \r\n    return \"&lt;html&gt;\" + myData + \"&lt;/html&gt;\"\r\n}\r\n\r\nprint(processData(nil))</code></pre><div><br></div>", user_id: 1, main_question_id: 2},
                     {content: "<pre><code>func processHtml(_ myData : String?) -&gt; String {\r\n    \r\n    guard let myData = myData else {\r\n        return \"&lt;html&gt;Data guard&lt;/html&gt;\"\r\n    }\r\n    \r\n    return \"&lt;html&gt;\" + myData + \"&lt;/html&gt;\"\r\n}\r\n\r\nprint(processHtml(\"\"))</code></pre><div><br></div>", user_id: 1, main_question_id: 2}
                 ])

Answer.create!([
  {content: "<div>&nbsp;Unknown<br>&nbsp;Spain</div><div>&nbsp;France</div><div>&nbsp;England</div>", point: 100, best: nil, question_id: 1},
  {content: "<div>&nbsp;Unknown</div><div>&nbsp;Optional(\"Spain\")</div><div>&nbsp;Optional(\"France\")</div><div>&nbsp;Optional(\"England\")</div>", point: 0, best: nil, question_id: 1},
  {content: "<div>&nbsp;nil</div><div>&nbsp;Spain</div><div>&nbsp;France</div><div>&nbsp;England</div>", point: 0, best: nil, question_id: 1},
  {content: "<div>&nbsp;Unknown</div><div>&nbsp;Unknown</div><div>&nbsp;Unknown</div><div>&nbsp;Unknown</div>", point: 0, best: nil, question_id: 1},
  {content: "<div>&nbsp;Unknown</div><div>&nbsp;Spain</div><div>&nbsp;Paris</div><div>&nbsp;England</div>", point: 100, best: nil, question_id: 2},
  {content: "<div>&nbsp;Unknown</div><div>&nbsp;Optional(\"Spain\")</div><div>&nbsp;Optional(\"Paris\")</div><div>&nbsp;Optional(\"England\")</div>", point: 0, best: nil, question_id: 2},
  {content: "<div>&nbsp;nil</div><div>&nbsp;Spain</div><div>&nbsp;Paris</div><div>&nbsp;England</div>", point: 0, best: nil, question_id: 2},
  {content: "<div>&nbsp;Unknown</div><div>&nbsp;Unknown</div><div>&nbsp;Unknown</div><div>&nbsp;Unknown</div>", point: 0, best: nil, question_id: 2},
  {content: "<div>&nbsp;Unknown</div><div>&nbsp;Spain</div><div>&nbsp;Paris</div><div>&nbsp;England</div>", point: 100, best: nil, question_id: 3},
  {content: "<div>&nbsp;Unknown</div><div>&nbsp;Optional(\"Spain\")</div><div>&nbsp;Optional(\"Paris\")</div><div>&nbsp;Optional(\"England\")</div>", point: 0, best: nil, question_id: 3},
  {content: "<div>&nbsp;nil</div><div>&nbsp;Spain</div><div>&nbsp;Paris</div><div>&nbsp;England</div>", point: 0, best: nil, question_id: 3},
  {content: "<div>&nbsp;Unknown</div><div>&nbsp;Unknown</div><div>&nbsp;Unknown</div><div>&nbsp;Unknown</div>", point: 0, best: nil, question_id: 3},
  {content: "<div>&lt;html&gt;In Data&lt;/html&gt;</div>", point: 100, best: nil, question_id: 4},
  {content: "<div>&lt;html&gt;Data guard&lt;/html&gt;</div>", point: 0, best: nil, question_id: 4},
  {content: "<div>Le code ne compile pas.</div>", point: 0, best: nil, question_id: 4},
  {content: "<div>Le code compile mais crash lors de l'execution.</div>", point: 0, best: nil, question_id: 4},
  {content: "<div>&lt;html&gt;Data guard&lt;/html&gt;</div>", point: 100, best: nil, question_id: 5},
  {content: "<div>&lt;html&gt;In Data&lt;/html&gt;</div>", point: -100, best: nil, question_id: 5},
  {content: "<div>&lt;html&gt;myData&lt;/html&gt;</div>", point: 0, best: nil, question_id: 5},
  {content: "<div>Le code compile mais crash lors de l'execution.</div>", point: 0, best: nil, question_id: 5},
  {content: "<div>&lt;html&gt;&lt;/html&gt;</div>", point: 100, best: nil, question_id: 6},
  {content: "<div>&lt;html&gt;myData&lt;/html&gt;</div>", point: 0, best: nil, question_id: 6},
  {content: "<div>&lt;html&gt;Data guard&lt;/html&gt;</div>", point: 0, best: nil, question_id: 6},
  {content: "<div>Le code compile mais crash lors de l'execution.</div>", point: 0, best: nil, question_id: 6}
])
