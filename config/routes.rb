Rails.application.routes.draw do
  resources :reports
  resources :candidates
  resources :job_offers
  resources :companies
  resources :questions
  resources :main_questions
  resources :scores
  resources :answers
  resources :pools
  resources :profiles

  resources :quizzes, except: [:create, :new, :edit]
  resources :meetings


  devise_for :users, controllers: { sessions: 'users/sessions', registrations: 'users/registrations'}
  devise_for :clients, controllers: { sessions: 'clients/sessions', registrations: 'clients/registrations'}
  devise_for :candidates, path: '', path_names: { registration: 'devisecreate'}


  get '/main_questions/preview/:id', to: 'main_questions#preview', as: 'main_questions_preview'


  #GET /[candidat_id]+[timestamp]+[client_id]/[quiz_id]/exam
  get   '/quizzes/:secret/:id/exam', to: 'quizzes#start_exam', as: 'start_exam'
  get   '/quizzes/:secret/:id/end_exam', to: 'quizzes#end_exam', as: 'end_exam'
  post  '/quizzes/:secret/:id/exam_question', to: 'quizzes#exam_question', as: 'exam_question'

  get   '/quizzes/:id/result_exam', to: 'quizzes#result_exam', as: 'result_exam'
  post  '/quizzes/send_result/:id', to: 'quizzes#send_result', as: 'send_result'


  get   '/quizzes/:candidate_id/new', to: 'quizzes#new', as: 'new_quiz'
  get   '/quizzes/:candidate_id/:id/edit', to: 'quizzes#edit', as: 'edit_quiz'
  post  '/quizzes/:candidate_id/new', to: 'quizzes#create'


  get   '/meetings/:candidate_id/book', to: 'meetings#book', as: 'book_meeting'
  post  '/meetings/book', to: 'meetings#save_book', as: 'save_book_meeting'


  get   '/candidates/select/:key_path', to: 'candidates#select_candidate', as: 'select_candidate'
  post  '/candidates/select/:key_path', to: 'candidates#selected_candidate'


  post '/meetings/forpool', to: 'meetings#meetings_for_pool', as: 'meetings_for_pool'

  get '/meetings/room/:id', to: 'meetings#candidate_meeting_room', as: 'meeting_room'
  get '/meetings/interview/room/:id', to: 'meetings#expert_meeting_room', as: 'expert_meeting_room'

  get '/dashboard', to: 'dashboard#dashboard'



  root to: 'dashboard#dashboard'



  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
