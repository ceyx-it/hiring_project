class MainQuestionsController < ApplicationController

  before_action :authenticate_user!
  before_action :set_main_question, only: [:show, :edit, :update, :destroy, :preview]

  # GET /main_questions
  # GET /main_questions.json
  def index
    if current_user.admin?
      @main_questions = MainQuestion.all
    else
      @main_questions = MainQuestion.where(pool_id: current_user.pool_ids)
    end
  end

  # GET /main_questions/1
  # GET /main_questions/1.json
  def show
  end

  #GET /main_questions/preview/1
  def preview
  end

  # GET /main_questions/new
  def new
    @main_question = MainQuestion.new
    @main_question.custom = false

    3.times do
      question = Question.new
      question.user = current_user
      4.times do
        answer = Answer.new
        question.answers << answer
      end
      @main_question.questions << question
    end
  end

  # GET /main_questions/1/edit
  def edit
  end

  # POST /main_questions
  # POST /main_questions.json
  def create
    @main_question = MainQuestion.new(main_question_params)

    respond_to do |format|
      if @main_question.save
        format.html { redirect_to @main_question, notice: 'Main question was successfully created.' }
        format.json { render :show, status: :created, location: @main_question }
      else
        format.html { render :new }
        format.json { render json: @main_question.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /main_questions/1
  # PATCH/PUT /main_questions/1.json
  def update
    respond_to do |format|
      if @main_question.update(main_question_params)
        format.html { redirect_to @main_question, notice: 'Main question was successfully updated.' }
        format.json { render :show, status: :ok, location: @main_question }
      else
        format.html { render :edit }
        format.json { render json: @main_question.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /main_questions/1
  # DELETE /main_questions/1.json
  def destroy
    @main_question.destroy
    respond_to do |format|
      format.html { redirect_to main_questions_url, notice: 'Main question was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share commons setup or constraints between actions.
    def set_main_question
      @main_question = MainQuestion.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def main_question_params
      params.require(:main_question).permit(:level, :custom, :subject, :pool_id, :data_state, questions_attributes: [:id, :content, :user_id, :main_question_id, answers_attributes: [:id, :content, :point, :best, :question_id]])
    end
end
