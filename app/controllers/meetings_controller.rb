class MeetingsController < ApplicationController
  before_action :set_meeting, only: [:show, :edit, :update, :destroy, :candidate_meeting_room, :expert_meeting_room]
  before_action :set_candidate, only: [:book, :save_book, :meetings_for_pool]
  before_action :authenticate_user!, only: [:index, :new, :edit, :create, :destroy, :expert_meeting_room]
  before_action :authenticate_candidate!, only: [:candidate_meeting_room]
  before_action :authenticate_client!, only: [:book, :save_book, :meetings_for_pool]

  # GET /meetings
  # GET /meetings.json
  def index
    @meetings = Meeting.all.order(:date_start)
    @new_meeting = Meeting.new

    @days = Array.new
    for i in 1..7 do
      @days << [(Time.now + i.day).strftime("%A %d %B %Y"), (Time.now + i.day).strftime("%Y-%m-%d")]
    end


    @hours = Array.new
    for h in 8..22 do
      m = 0
      while m <= 45
        @hours << [" #{h.to_s}h#{m.to_s}", "#{h.to_s}:#{m.to_s}"]
        m += 15
      end
    end
  end

  # GET /meetings/1
  # GET /meetings/1.json
  def show
  end

  # GET /meetings/new
  def new
    @meeting = Meeting.new
  end

  # GET /meetings/1/edit
  def edit
  end

  # POST /meetings
  # POST /meetings.json
  def create
    @meeting = Meeting.new(meeting_params)
    @meeting.report = Report.new

    @meeting.date_start = (params[:meeting][:day] + " " + params[:meeting][:hour]).to_datetime
    respond_to do |format|
      if @meeting.save
        format.html { redirect_to meetings_url, notice: 'Meeting was successfully created.' }
        format.json { render :show, status: :created, location: @meeting }
      else
        format.html { render :new }
        format.json { render json: @meeting.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /meetings/1
  # PATCH/PUT /meetings/1.json
  def update
    respond_to do |format|
      if @meeting.update(meeting_params)
        format.html { redirect_to @meeting, notice: 'Meeting was successfully updated.' }
        format.json { render :show, status: :ok, location: @meeting }
      else
        format.html { render :edit }
        format.json { render json: @meeting.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /meetings/1
  # DELETE /meetings/1.json
  def destroy
    @meeting.destroy
    respond_to do |format|
      format.html { redirect_to meetings_url, notice: 'Meeting was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def book
    @meetings = Meeting.where(data_state: Meeting.data_states[:free])
  end

  def save_book
    @meeting = Meeting.find(params[:meeting_id])
    @meeting.candidate_id = @candidate.id
    @meeting.data_state = :booked

    generate_token
    @meeting.room = @session_id
    @meeting.candidate_token_id = @candidate_token
    @meeting.expert_token_id = @expert_token
    @meeting.comment = params[:comment]

    CandidatesMailer.confirmed_meeting(@meeting).deliver

    respond_to do |format|
      if @meeting.save
        format.html { redirect_to @meeting, notice: 'Meeting was successfully created.' }
        format.json { render :show, status: :created, location: @meeting }
      else
        format.html { render :book, notice: "Echec de la réservation" }
        format.json { render json: @meeting.errors, status: :unprocessable_entity }
    end
      end
  end

  def meetings_for_pool
    @pool = Pool.find(params[:pool_id])
    @meetings = Meeting.where(data_state: Meeting.data_states[:free]).order(:date_start)
  end

  def expert_meeting_room
    @api_key = ENV['OT_API_KEY']

  end

  def candidate_meeting_room
    @api_key = ENV['OT_API_KEY']
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_meeting
      @meeting = Meeting.find(params[:id])
    end

    def set_candidate
      @candidate = Candidate.find(params[:candidate_id])
    end

    def generate_token
      opentok = OpenTok::OpenTok.new ENV['OT_API_KEY'], ENV['OT_SECRET']
      session = opentok.create_session
      session = opentok.create_session :archive_mode => :always, :media_mode => :routed
      @session_id = session.session_id


      @candidate_token = session.generate_token({
                                         :expire_time => Time.now.to_i+(7 * 24 * 60 * 60), # in one week
                                         :data => 'expert'
                                     });

      @expert_token = session.generate_token({
                                          :expire_time => Time.now.to_i+(7 * 24 * 60 * 60), # in one week
                                          :data => 'candidate'
                                      });

    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def meeting_params
      params.require(:meeting).permit(:candidate_id, :report_id, :user_id, :date_start, :date_start_meeting, :date_end_meeting, :room, :data_state)
    end
end
