class CandidatesController < ApplicationController
  before_action :set_candidate, only: [:show, :edit, :update, :destroy, :selected_candidate]
  before_action :create_new_candidate, only: [:create]
  before_action :set_select_candidate, only: [:create, :select_candidate]

  before_action :authenticate_client!, only: [:index, :show, :new, :edit, :create, :destroy, :select_candidate, :selected_candidate]

  # GET /candidates
  # GET /candidates.json
  def index
    @candidates = Candidate.all
  end

  # GET /candidates/1
  # GET /candidates/1.json
  def show
    @quizzes = Quiz.where(candidate_id: @candidate.id)
    @meetings = Meeting.where(candidate_id: @candidate.id)
  end

  # GET /candidates/new
  def new
    @candidate = Candidate.new
    @candidate.profile = Profile.new
  end

  # GET /candidates/1/edit
  def edit
  end

  # POST /candidates
  def create
    respond_to do |format|
      if @candidate.save
        if @key_path.present?

          if @key_path == NEW_MEETING_ACTION
            format.html { redirect_to new_meeeting_path(candidate_id:  @candidate.id) }
          else
            format.html { redirect_to new_quiz_path(candidate_id:  @candidate.id) }
          end

        else
          format.html { redirect_to @candidate, notice: 'Candidate was successfully created.' }
          format.json { render :show, status: :created, location: @candidate }
        end
      else
        if @key_path.present?
            format.html { render :select_candidate, key_path: @key_path }
        else
          format.html { render :new }
          format.json { render json: @candidate.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /candidates/1
  # PATCH/PUT /candidates/1.json
  def update
    respond_to do |format|
      if @candidate.update(candidate_params)
          format.html { redirect_to @candidate, notice: 'Candidate was successfully updated.' }
          format.json { render :show, status: :ok, location: @candidate }
      else
          format.html { render :edit }
          format.json { render json: @candidate.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /candidates/1
  # DELETE /candidates/1.json
  def destroy
    @candidate.destroy
    respond_to do |format|
      format.html { redirect_to candidates_url, notice: 'Candidate was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def select_candidate
  end

  def selected_candidate
    set_key_path
    Rails.logger.debug "KEY = PATH #{set_key_path}"
    respond_to do |format|
      if @key_path == NEW_QUIZ_ACTION
        format.html { redirect_to new_quiz_path(candidate_id:  @candidate.id) }
      else
        format.html { redirect_to book_meeting_path(candidate_id:  @candidate.id) }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_candidate
      @candidate = Candidate.find(params[:id])
    end

    def set_key_path
      @key_path = params[:key_path]
    end

    def set_select_candidate
      if params[:key_path].present?
        @candidates = Candidate.where(data_state: Candidate.data_states[:active])
        set_key_path
        if @candidate.nil?
          @candidate = Candidate.new
          @candidate.profile = Profile.new
        end
      end
    end

    def create_new_candidate
      @candidate = Candidate.new(candidate_params)
      generated_password = Devise.friendly_token.first(8)
      @candidate.password = generated_password
      CandidatesMailer.welcome_candidate(@candidate, generated_password).deliver
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def candidate_params
      params.require(:candidate).permit(:experience, :email, :data_state, :key_path, profile_attributes: [:first_name, :last_name, :gender, :phone])
    end
end
