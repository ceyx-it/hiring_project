class QuizzesController < ApplicationController
  before_action :set_quiz, only: [:show, :edit, :update, :destroy, :send_result, :result_exam]
  before_action :set_candidate, only: [:new, :create]
  before_action :authenticate_client!, except: [:exam_question, :start_exam, :end_exam]
  before_action :prepare_exam, only: [:exam_question, :start_exam, :end_exam]

  layout "fullwidth", only: [:exam_question, :start_exam, :end_exam]

  # GET /quizzes
  # GET /quizzes.json
  def index
    @quizzes = Quiz.all
  end

  # GET /quizzes/1
  # GET /quizzes/1.json
  def show
  end

  # GET /quizzes/:candidate_id/new
  def new
    @quiz = Quiz.new
    @quiz.candidate = @candidate
    @quiz.client = current_client
  end

  # GET /quizzes/1/:candidate_id/edit
  def edit
    @candidate = @quiz.candidate
  end

  # POST /quizzes/:candidate_id
  def create
    @quiz = Quiz.new(quiz_params)

    respond_to do |format|
      if @quiz.save
        format.html { redirect_to @quiz, notice: 'Quiz was successfully created.' }
        format.json { render :show, status: :created, location: @quiz }
      else
        format.html { render :new }
        format.json { render json: @quiz.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /quizzes/1
  def update
    respond_to do |format|
      if @quiz.update(quiz_params)
        format.html { redirect_to @quiz, notice: 'Quiz was successfully updated.' }
        format.json { render :show, status: :ok, location: @quiz }
      else
        format.html { render :edit }
        format.json { render json: @quiz.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /quizzes/1
  # DELETE /quizzes/1.json
  def destroy
    @quiz.destroy
    respond_to do |format|
      format.html { redirect_to quizzes_url, notice: 'Quiz was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  ###
  #  PASS QUIZ
  ###

  #GET /[candidat_id]+[timestamp]+[client_id]/[quiz_id]/exam
  def start_exam
  end

  #GET /[candidat_id]+[timestamp]+[client_id]/[quiz_id]/end_exam
  def end_exam
    @quiz.completed!
  end

  def result_exam

  end

  #GET /[candidat_id]+[timestamp]+[client_id]/[quiz_id]/exam_question
  def exam_question
    @quiz.in_progress!
    if params[:question_id] != nil
      Rails.logger.debug("My object: #{params[:quiz][:answer_ids]}")
      score = Score.new
      score.question = Question.find(params[:question_id])

      params['quiz']['answer_ids'].each do | id_params|
        id = id_params
        id["quiz_answer_ids_"] = ''
        score.answers << Answer.find(id)
      end
      @quiz.scores << score
    end

    @question = @quiz.next_question
    if @question.nil?
      @quiz.calculate_score

      @quiz.save
      redirect_to end_exam_path(:secret => @quiz.tag_secret, :id => @quiz.id)
      return
    end

    respond_to do |format|
      format.html { render :exam_question }
    end
  end

  def send_result
    ResultMailer.result_email(@quiz).deliver
    redirect_to result_exam_path(:id => @quiz.id)
  end

  private

    def set_candidate
      @candidate = Candidate.find(params[:candidate_id])
    end

    def prepare_exam
      set_quiz
      if @quiz.tag_secret != params[:secret]
        head(404)
      end
    end

    def set_quiz
      @quiz = Quiz.find(params[:id])
    end

    def quiz_params
      # params.require(:quiz).permit(:score_final, :score_max, :pool_id, :client_id, candidate_attributes:[:experience, :email, :data_state, profile_attributes: [:first_name, :last_name]])
      params.require(:quiz).permit(:score_final, :score_max, :pool_id, :client_id, :candidate_id)
    end
end

=begin

    generated_password = Devise.friendly_token.first(8)
    newCandidate = Candidate.new(:email => @quiz.candidate.email, :password => generated_password)
    newCandidate.experience = @quiz.candidate.experience
    newCandidate.profile = @quiz.candidate.profile
    @quiz.candidate = newCandidate
    CandidatesMailer.welcome_candidate(@quiz, generated_password).deliver

=end
