class Quiz < ApplicationRecord
  after_initialize :set_default_value
  belongs_to :pool
  belongs_to :client
  belongs_to :candidate

  accepts_nested_attributes_for :candidate

  has_many :scores

  enum data_state: [:pending, :in_progress, :completed]

  def set_default_value
    self.data_state ||= :pending
  end

  def next_question
    main_questions = MainQuestion.where(pool_id: self.pool_id)

    main_questions.each do |main_question|
      unless self.already_use? main_question
        return main_question.questions.sample
      end
    end
    nil
  end

  def already_use? (main_question)
    Rails.logger.debug("main_question_id #{main_question.id}")
    self.scores.each do |score|
      Rails.logger.debug("score.question.main_question_id #{score.question.main_question_id}")
      if score.question.main_question_id == main_question.id
        return true
      end
    end
    false
  end

  def tag_secret
    self.created_at.day.to_s + self.client.id.to_s + "4" + self.created_at.sec.to_s + self.created_at.year.to_s  + "2" +  self.candidate.id.to_s + self.created_at.month.to_s + self.id.to_s + self.created_at.hour.to_s + self.created_at.min.to_s
  end

  def calculate_score
    result = 0
    self.scores.each do |score|
      result += score.point
    end
    self.score_final = result / 100
    self.score_max = self.scores.count
  end

end
