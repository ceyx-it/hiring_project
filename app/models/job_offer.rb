class JobOffer < ApplicationRecord
  has_many :candidates
  belongs_to :client
end
