class Meeting < ApplicationRecord
  belongs_to :candidate, optional: true
  belongs_to :user, optional: true
  belongs_to :report, optional: true

  after_initialize :set_default_data_state, :if => :new_record?


  enum data_state: [:free, :booked, :confirmed, :finish, :archived]


  def set_default_data_state
    self.data_state ||= :free
  end

end
