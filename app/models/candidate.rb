class Candidate < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  enum data_state: [:active, :archive]
  enum experience: [:junior, :confirm, :senior]
  after_initialize :set_default_data_state, :if => :new_record?

  belongs_to :profile
  accepts_nested_attributes_for :profile

  def set_default_data_state
    self.data_state ||= :active
    self.experience ||= :junior
  end

end
