class Question < ApplicationRecord
  belongs_to :user
  belongs_to :main_question
  has_many :answers

  accepts_nested_attributes_for :answers
  before_save :update_content

  def update_content
    if self.content.include? "<pre>"
      self.content = self.content.gsub("<pre>", "<pre><code>")
      self.content = self.content.gsub("</pre>", "</code></pre>")
    end
  end
end
