class Profile < ApplicationRecord

  def full_name
    full_name = ""
    if self.gender.present?
      full_name = self.gender + " "
    end
    if self.first_name.present?
      full_name = full_name +  self.first_name + " "
    end
    if self.last_name.present?
      full_name = full_name + self.last_name
    end
    full_name.titleize
  end

end
