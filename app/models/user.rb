class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable

  enum role: [:user, :admin, :user_author, :user_expert]
  after_initialize :set_default_role, :if => :new_record?

  has_and_belongs_to_many :pools

  belongs_to :profile

  def set_default_role
    self.role ||= :user
  end

end
