class Answer < ApplicationRecord
  belongs_to :question

  before_save :update_content
  after_initialize :set_default_value

  has_and_belongs_to_many :scores


  def set_default_value
    self.point ||= 0
  end

  def update_content
    if self.content.include? "<pre>"
      self.content = self.content.gsub("<pre>", "<pre><code>")
      self.content = self.content.gsub("</pre>", "</code></pre>")
    end
  end

end


