class Score < ApplicationRecord
  belongs_to :question
  belongs_to :quiz
  has_and_belongs_to_many :answers

  def point
    success = true
    point = 0
    self.answers.each do |answer|
      if answer.point < 0
        return answer.point
      end
      if answer.point == 0
        point = 0
        success = 0
      end
      if answer.point > 0 && success
        point += answer.point
      end
    end
    success ? point : 0
  end


end
