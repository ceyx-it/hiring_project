class Pool < ApplicationRecord
  after_initialize :set_default_value
  has_many :main_questions
  has_and_belongs_to_many :users

  enum data_state: [:work, :prod]

  def set_default_value
    self.data_state ||= :prod
  end

end
