class MainQuestionValidator < ActiveModel::Validator

  def validate(record)
    record.questions.each do |question|
      check_question(question, record)
    end
  end


  def check_question(question, record)
    error_point = 0

    arr_content = []
    question.answers.each do |answer|
      if answer.point > 0
        error_point += answer.point
      end
      arr_content << answer.content
    end

    unless record.in_progress? && record.work?
      if error_point != 100
        record.errors[:point] << "Le cumul des bonnes réponses doit être égale à 1."
      end

      if arr_content.uniq.length < 4
        record.errors[:content_answer] << "Pour chaque question il doit avoir au minimum 4 réponses différentes."
      end

      if question.content.empty?
        record.errors[:content_question_empty] << "Toute les varaintes doivent avoir... une question."
      elsif question.content.length < 20
        record.errors[:size_question] << "La question doit au moins faire 20 caractères."
      end
    end

 end
end


class MainQuestion < ApplicationRecord
  validates_with MainQuestionValidator
  after_initialize :set_default_value
  belongs_to :pool
  has_many :questions
  accepts_nested_attributes_for :questions

  enum level: [:junior, :confirm, :senior]
  enum data_state: [:work, :in_progress, :wait_validation, :ready, :prod]

  def set_default_value
    self.data_state ||= :work
    self.level ||= :junior
  end

  def level_to_s
    str = "Junior"
    if self.confirm?
      str = "Confirmé"
    elsif self.senior?
      str = "Senior"
    end
    str
  end


  def data_state_to_s
    if self.work?
      return "A faire"
    elsif self.in_progress?
      return "En cours"
    elsif self.wait_validation?
      return "Acceptance"
    elsif self.ready?
      return "Validé"
    elsif self.prod?
      return "En Production"
    end
  end


end
