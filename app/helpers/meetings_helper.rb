module MeetingsHelper


  def self.data_state_to_s meeting
    if meeting.free?
      return "Disponible"
    elsif meeting.booked?
      return "Réservé"
    elsif meeting.confirmed?
      return "Confirmé"
    elsif meeting.archived?
      return "Archivé"
    end
    return "Terminé"
  end

end
