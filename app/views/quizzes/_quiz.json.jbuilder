json.extract! quiz, :id, :score_final, :score_max, :pool_id, :client_id, :candidate_id, :created_at, :updated_at
json.url quiz_url(quiz, format: :json)
