json.extract! candidate, :id, :experience, :email, :profile_id, :data_state, :created_at, :updated_at
json.url candidate_url(candidate, format: :json)
