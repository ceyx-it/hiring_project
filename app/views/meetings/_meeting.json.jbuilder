json.extract! meeting, :id, :candidate_id, :report_id, :user_id, :date_start, :date_start_meeting, :date_end_meeting, :room, :data_state, :created_at, :updated_at
json.url meeting_url(meeting, format: :json)
