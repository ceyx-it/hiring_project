json.extract! company, :id, :name, :activate, :created_at, :updated_at
json.url company_url(company, format: :json)
