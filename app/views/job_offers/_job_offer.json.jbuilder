json.extract! job_offer, :id, :code_offer, :title, :comment, :web_link, :email, :finish, :created_at, :updated_at
json.url job_offer_url(job_offer, format: :json)
