json.extract! question, :id, :content, :pool_id, :user_id, :main_question_id, :created_at, :updated_at
json.url question_url(question, format: :json)
