json.extract! pool, :id, :subject, :created_at, :updated_at
json.url pool_url(pool, format: :json)
