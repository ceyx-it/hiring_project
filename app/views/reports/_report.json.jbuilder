json.extract! report, :id, :advise, :best_point, :bad_point, :data_state, :meeting_id, :created_at, :updated_at
json.url report_url(report, format: :json)
