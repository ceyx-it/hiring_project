json.extract! main_question, :id, :level, :custom, :subject, :pool_id, :created_at, :updated_at
json.url main_question_url(main_question, format: :json)
