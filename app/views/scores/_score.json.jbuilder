json.extract! score, :id, :timer, :answer_id, :question_id, :quiz_id, :created_at, :updated_at
json.url score_url(score, format: :json)
