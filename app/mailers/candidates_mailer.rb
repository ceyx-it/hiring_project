class CandidatesMailer < ApplicationMailer
  default from: "noreply@ceyx.fr"

  def welcome_candidate(candidate, password)
    @candidate = candidate
    @password = password
    mail(to: @candidate.email, subject: 'Tout pour votre entretien!')
  end

  def confirmed_meeting(meeting)
    @meeting = meeting
    mail(to: @meeting.candidate.email, subject: 'Un nouveau rendez-vous Expert.')
  end

end
