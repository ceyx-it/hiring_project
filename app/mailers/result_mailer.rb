class ResultMailer < ApplicationMailer
  default from: "noreply@ceyx.fr"

  def result_email(quiz)
    @quiz = quiz
    @candidate = quiz.candidate
    mail(to: @candidate.client.email, subject: 'Sample Email!!')
  end

end
